const config = {
  app: {
    port: process.env.PORT || 3000
  },
  db: {
    host: process.env.OASIS_DB_HOST,
    port: process.env.OASIS_DB_PORT|| 5432,
    name: process.env.OASIS_DB_NAME,
    user: process.env.OASIS_DB_USER,
    password: process.env.OASIS_DB_PWD
  }
 };

 module.exports = config;
