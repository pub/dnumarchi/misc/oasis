/* This code snippet is a route handler in a Node.js application using Express framework. */
var express = require('express');
var router = express.Router();
const config = require('../config');

/* GET users listing. */
router.post('/', function(req, res, next) {

  console.log(req.body);
  console.log(config);
  
  // Connexion à la base
  const { Client } = require('pg')
  const client = new Client({
    host: config.db.host,
    port: config.db.port,
    database: config.db.name,
    user: config.db.user,
    password: config.db.password
  })

  client.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });



  // Requête

  const id_thematique = req.body.id_thematique;
  console.log('id_thematique : ', id_thematique);
  var requete = `SELECT nom_couche, id_couche FROM public."InfoCouches" WHERE id_thematique = ${id_thematique}`;

  client.query(requete, (err, result) => {
      console.log(result);
      res.json(result.rows);
      client.end();
  });

});

module.exports = router;
