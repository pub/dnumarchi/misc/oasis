/* This code snippet is a Node.js Express route that handles a POST request.*/
var express = require('express');
var router = express.Router();
const config = require('../config');

/* GET users listing. */
router.post('/', function(req, res, next) {

  console.log(req.body);
  
  // Connexion à la base
  const { Client } = require('pg')
  const client = new Client({
    host: config.db.host,
    port: config.db.port,
    database: config.db.name,
    user: config.db.user,
    password: config.db.password
  })

  client.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });



  // Requête
  /* This part of the code snippet is extracting the `id_couche` value from the request body and then
  constructing a SQL query to fetch data from the database based on that `id_couche`. */
  const { id_couche } = req.body;

  var requete = `SELECT public."Thematiques".nom_thematique, public."InfoCouches".nom_couche, public."InfoCouches".id_thematique, public."InfoCouches".cout, public."InfoCouches".date, public."InfoCouches".description_generale, public."InfoCouches".revisite, public."InfoCouches".lien_applisat, public."InfoCouches".obtention_couche, public."InfoCouches".resolution_spatio, public."InfoCouches".applications, public."InfoCouches".limites, public."InfoCouches".biblio, public."InfoCouches".satellite, public."InfoCouches".bandes, public."InfoCouches".id_couche
  FROM public."InfoCouches" 
  JOIN public."Thematiques" ON public."InfoCouches".id_thematique = public."Thematiques".id_themathique
  WHERE public."InfoCouches".id_couche =` + id_couche;

  client.query(requete, (err, result) => {
      console.log(result);
      res.json(result.rows);
      client.end();
  });

});

module.exports = router;
