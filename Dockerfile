ARG NODE_VERSION=20.12.2-alpine3.19

FROM node:${NODE_VERSION}
ENV NODE_ENV production

WORKDIR /app
COPY . .
RUN npm ci --only=production

USER node
CMD node bin/www
